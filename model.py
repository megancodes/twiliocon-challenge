import json
from urllib2 import urlopen
from math import floor

""" 
This is some python code that calls the Hacker Olympics JSON API
"""

def load_leaderboard():
    lboard = urlopen("http://judging.thehackerolympics.com/leaderboard.json")
    lboard_data = json.load(lboard)
    return lboard_data
        

### End class declarations


def main():
    """In case we need this for something"""
    pass

if __name__ == "__main__":
    main()
